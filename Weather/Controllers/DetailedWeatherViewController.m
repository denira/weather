#import "DetailedWeatherViewController.h"
#import "HoursTVCell.h"
#import "DetailedWeatherVCModel.h"
#import "Helper.h"

@interface DetailedWeatherViewController () <UITableViewDataSource, UITableViewDelegate, CommonVCModelDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UIView *errorView;

@property NSString * date;
@property DetailedWeatherVCModel * model;

@end

@implementation DetailedWeatherViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.model = [[DetailedWeatherVCModel alloc] initWithDelegate:self];
    self.model.date = self.date;
    [self.model update];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = self.date;
    self.tableView.hidden = YES;
    [self.loader startAnimating];
    self.errorView.hidden = YES;
}

+ (void)cast:(UIViewController *)oldVC withDate:(NSString *)date
{
    DetailedWeatherViewController * vc = (DetailedWeatherViewController *)oldVC;
    vc.date = date;
}

- (IBAction)update
{
    self.errorView.hidden = YES;
    self.loader.hidden = NO;
    [self.model update];
}
#pragma UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.model.weathers.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HoursTVCell *cell= [tableView dequeueReusableCellWithIdentifier:@"hourCell"];
    cell.weather = [self.model.weathers objectAtIndex:indexPath.row];
    return cell;
}

#pragma UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma DetailedWeatherTVCModelDelegate

- (void)modelDidFailUpdate
{
    __weak typeof(self) this = self;
    [Helper runOnMainThread:^{
        this.errorView.hidden = NO;
        this.tableView.hidden = YES;
        this.loader.hidden = YES;
    }];
}

- (void)modelDidUpdate {
    __weak typeof(self) this = self;
    [Helper runOnMainThread:^{
        this.tableView.hidden = NO;
        [this.tableView reloadData];
        this.loader.hidden = YES;
    }];
}

@end

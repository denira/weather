#import "GeneralViewController.h"
#import "DayTVCell.h"
#import "Constants.h"
#import "DetailedWeatherViewController.h"
#import "GeneralVCModel.h"
#import "Weather.h"
#import "Helper.h"
#import "GeolocationManager.h"
#import "Locality.h"

@interface GeneralViewController ()  <UITableViewDataSource, UITableViewDelegate, CommonVCModelDelegate, GeolocationManagerDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UILabel *tempLabel;
@property (weak, nonatomic) IBOutlet UILabel *windLabel;
@property (weak, nonatomic) IBOutlet UILabel *pressureLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIImageView *weatherIcon;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UIView *currentDayView;
@property (weak, nonatomic) IBOutlet UIView *errorView;

@property (weak) UIAlertController * alertVC;

@property GeneralVCModel * model;
@property BOOL isNeedUpdate;
@end

@implementation GeneralViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.model = [[GeneralVCModel alloc] initWithDelegate:self];
    [self.model update];
    self.isNeedUpdate = YES;
    
    self.table.dataSource = self;
    self.table.delegate = self;
    
    [GeolocationManager.shared setDelegate:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateModel:) name:@"Select city" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.isNeedUpdate)
    {
        self.errorView.hidden = YES;
        self.title = [Helper getSavedLocality].name;
        self.title = [[self.title componentsSeparatedByString:@","] firstObject];
        if (!self.title) self.title = DEFAULT_CITY;
        self.currentDayView.hidden = YES;
        [self.loader startAnimating];
    }
}

-(void)showWeatherToday:(Weather *)weather
{
    self.descLabel.text = weather.weatherDesc;
    self.tempLabel.text =[NSString stringWithFormat:@"%0.1f°С", weather.temp];
    self.windLabel.text = [NSString stringWithFormat:@"%0.2f км/ч", weather.wind];
    self.pressureLabel.text = [NSString stringWithFormat:@"%0.2f гПа", weather.pressure];
    self.currentDayView.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    self.isNeedUpdate = NO;
    [super viewWillDisappear:animated];
}
- (IBAction)update
{
    self.errorView.hidden = YES;
    self.loader.hidden = NO;
    [self.model update];
}

- (void)updateModel:(NSNotification *)note
{
    Locality * locality = note.object;
    self.title = [[locality.name componentsSeparatedByString:@","] firstObject];
    self.currentDayView.hidden = YES;
    [self.loader startAnimating];
    self.loader.hidden = NO;
    [self.model update];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *newController = segue.destinationViewController;
    if ([newController isMemberOfClass:[DetailedWeatherViewController class]])
    {
        Weather * selectDayWeather;
        if ([sender isKindOfClass:DayTVCell.class])
        {
            DayTVCell * cell = (DayTVCell *)sender;
            selectDayWeather = cell.weather;
        }
        else selectDayWeather = self.model.weathers.firstObject;
        
        [DetailedWeatherViewController cast:newController withDate:selectDayWeather.date];
    }
}

- (IBAction)showMoreForToday
{
    [self performSegueWithIdentifier:@"showMore" sender:@(0)];
}

- (IBAction)getCoordinates:(id)sender
{
    [GeolocationManager.shared updateCoords];
}

#pragma UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.model.weathers.count - 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DayTVCell* cell= [tableView dequeueReusableCellWithIdentifier:@"dayCell"];
    cell.weather = [self.model.weathers objectAtIndex:(indexPath.row + 1)];
    return cell;
}

#pragma UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self performSegueWithIdentifier:@"showMore" sender:[tableView cellForRowAtIndexPath:indexPath]];
}

#pragma CommonVCModelDelegate

- (void)modelDidUpdate
{
    __weak typeof(self) this = self;
    [Helper runOnMainThread:^{
        [this showWeatherToday:self.model.weatherToday];
        this.table.hidden = NO;
        [this.table reloadData];
        this.currentDayView.hidden = NO;
        this.loader.hidden = YES;
    }];
    [self.model.weatherToday loadImageOnSuccess:^(NSData * _Nonnull data) {
        UIImage * image = [UIImage imageWithData:data];
        [Helper runOnMainThread:^{
            this.weatherIcon.image = image;
        }];
    } onFail:^(NSError * _Nonnull error) {}];
}

- (void)modelDidFailUpdate
{
    __weak typeof(self) this = self;
    [Helper runOnMainThread:^{
        this.errorView.hidden = NO;
        this.loader.hidden = YES;
        this.currentDayView.hidden = YES;
        this.table.hidden = YES;
    }];
}

#pragma GeolocationManagerDelegate

- (void)managerDidStartUpdate {
    self.isNeedUpdate = YES;
    self.alertVC = [UIAlertController alertControllerWithTitle:@"Определение местоположения..."
                                                       message:@""
                                                preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:self.alertVC animated:YES completion:nil];
}


- (void)managerStatusIsDenied {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Геолокация запрещена"
                                                                   message:@"Разрешить или запретить определение местоположения можно в настройках"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    UIAlertAction* settingsAction = [UIAlertAction actionWithTitle:@"Настройки" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
                                                          }];
    
    [alert addAction:defaultAction];
    [alert addAction:settingsAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)coordinatesDidFailReceived {
    __weak typeof(self) this = self;
    [self.alertVC dismissViewControllerAnimated:NO completion:^{
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Не удалось определить местоположение"
                                                                       message:@""
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        [alert addAction:defaultAction];
        
        [this presentViewController:alert animated:YES completion:nil];
    }];
}

- (void)coordinatesDidReceived
{
    if (self.isNeedUpdate)
    {
        self.isNeedUpdate = NO;
        
        __weak typeof(self) this = self;
        [Helper runOnMainThread:^{
            [self.alertVC dismissViewControllerAnimated:YES completion:^{
                this.title = [Helper getSavedLocality].name;
                this.currentDayView.hidden = YES;
                this.table.hidden = YES;
                this.loader.hidden = NO;
                [this.model update];
            }];
        }];
    }
}

@end

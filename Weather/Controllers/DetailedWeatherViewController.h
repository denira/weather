#import <UIKit/UIKit.h>

@interface DetailedWeatherViewController : UIViewController

+ (void)cast:(UIViewController *)oldVC withDate:(NSString *)date;

@end

#import "LocalitySearchViewController.h"
#import "LocalitySearchVCModel.h"
#import "Helper.h"
#import "Locality.h"

@interface LocalitySearchViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, CommonVCModelDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *errorView;
@property (weak, nonatomic) IBOutlet UILabel *emptyResultLabel;

@property LocalitySearchVCModel * model;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableBottomConstraint;
@end

@implementation LocalitySearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.searchBar.delegate = self;
    
    self.model = [[LocalitySearchVCModel alloc] initWithDelegate:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.loader.hidden = YES;
    self.errorView.hidden = YES;
    self.emptyResultLabel.hidden = YES;
}

- (IBAction)close
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)update
{
    self.loader.hidden = NO;
    self.errorView.hidden = YES;
    [self.model update];
}

- (void)moveViewBottomBorder
{
    CGRect frame = self.tableView.frame;
    CGFloat diff = self.tableView.contentSize.height - frame.size.height;
    self.tableBottomConstraint.constant += diff;
    if (self.tableBottomConstraint.constant < 200) self.tableBottomConstraint.constant = 200;
    
    NSInteger topBorder = [UIScreen mainScreen].bounds.size.height - 136;
    if (self.tableBottomConstraint.constant > topBorder) self.tableBottomConstraint.constant = topBorder;
}

#pragma UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cityCell"];
    Locality * locality = [self.model.localities objectAtIndex:indexPath.row];
    cell.textLabel.text = locality.name;
    cell.textLabel.numberOfLines = 0;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.model.localities.count;
}

#pragma UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    Locality * locality = [self.model.localities objectAtIndex:indexPath.row];
    if (locality)
    {
        [Helper saveLocality:locality];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Select city" object:locality];
        [self close];
    }
}

#pragma UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    self.model.localityName = searchBar.text;
    [self.model update];
    
    [self.searchBar resignFirstResponder];
    [self.tableView reloadData];
    
    self.loader.hidden = NO;
    [self.loader startAnimating];
    
    self.emptyResultLabel.hidden = YES;
    [self moveViewBottomBorder];
}

- (void)modelDidFailUpdate
{
    __weak typeof(self) this = self;
    [Helper runOnMainThread:^{
        [this.tableView reloadData];
        this.loader.hidden = YES;
        this.errorView.hidden = NO;
        [this moveViewBottomBorder];
    }];
}

- (void)modelDidUpdate {
    __weak typeof(self) this = self;
    [Helper runOnMainThread:^{
        [this.tableView reloadData];
        this.loader.hidden = YES;
        this.errorView.hidden = YES;
        if (!this.model.localities.count) this.emptyResultLabel.hidden = NO;
        [this moveViewBottomBorder];
    }];
}

@end

#import <Foundation/Foundation.h>
#import "Helper.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetworkRequest : NSMutableURLRequest

@property (copy) XDataBlock onSuccess;
@property (copy) XErrorBlock onFail;

- (id)initWithURL:(NSURL *)URL withType:(NSString *)type;
- (void)start;

@end

NS_ASSUME_NONNULL_END

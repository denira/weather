#import "Helper.h"
#import "Locality.h"

@implementation Helper

+ (void)runOnMainThread:(dispatch_block_t)block
{
    if ([NSThread isMainThread]) {
        block();
    }
    else {
        dispatch_async(dispatch_get_main_queue(),  block);
    }
}

+ (Locality *)getSavedLocality
{
    NSString *name = [[NSUserDefaults standardUserDefaults] objectForKey:@"locality"];
    NSString *coords = [[NSUserDefaults standardUserDefaults] objectForKey:@"coords"];
    return  [[Locality alloc] initWithName:name withCoords:coords];
}

+ (void)saveLocality:(Locality *)locality
{
    [[NSUserDefaults standardUserDefaults] setObject:locality.name forKey:@"locality"];
    [[NSUserDefaults standardUserDefaults] setObject:locality.coords forKey:@"coords"];
}
@end

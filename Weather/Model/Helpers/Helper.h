#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define CALL(Block, ...) ({ (Block != NULL) ? Block(__VA_ARGS__) : (typeof(Block(__VA_ARGS__)))0; })

typedef void (^XBlock)(void);
typedef void (^XDataBlock)(NSData * data);
typedef void (^XErrorBlock)(NSError * error);

@class Locality;

@interface Helper : NSObject

+ (void)runOnMainThread:(dispatch_block_t)block;
+ (Locality *)getSavedLocality;
+ (void)saveLocality:(Locality *)locality;

@end

NS_ASSUME_NONNULL_END

#import "NetworkRequest.h"

#define RETRY_COUNT 3

@interface NetworkRequest ()

@property NSInteger retryNumber;
@property BOOL isProcessing;

@end

@implementation NetworkRequest

- (id)initWithURL:(NSURL *)URL withType:(NSString *)type 
{
    if ((self = [super initWithURL:URL]))
    {
        self.retryNumber = 0;
        self.timeoutInterval = 10;
        self.HTTPMethod = type;
    }
    
    return self;
}

- (void)start
{
    if (self.isProcessing) return;
    [self _start];
}

- (void)_start
{
    ++ self.retryNumber;
    
    self.isProcessing = YES;
    
    __weak typeof(self) this = self;
    NSURLSessionDataTask * dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:self completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        this.isProcessing = NO;
        if (data) CALL(self.onSuccess, data);
        else [this processError:error info:error.userInfo canRetry:YES];
    }];
    [dataTask resume];
}

- (void)processError:(NSError *)error info:(NSDictionary *)errDic canRetry:(BOOL)canRetry
{
    if (!canRetry || self.retryNumber >= RETRY_COUNT)
    {
        CALL(self.onFail,(error?:[[NSError alloc] initWithDomain:errDic[@"error_message"] code:[errDic[@"error_code"] integerValue] userInfo:errDic]));
        return;
    }
    
    [self _start];
}

@end

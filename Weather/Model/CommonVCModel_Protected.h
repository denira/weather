#import <Foundation/Foundation.h>
#import "CommonVCModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommonVCModel ()

@property NetworkManager * networkManager;
@property (weak) id<CommonVCModelDelegate> delegate;

@end

NS_ASSUME_NONNULL_END

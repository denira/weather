#import "LocalitySearchVCModel.h"
#import "NetworkManager.h"
#import "Locality.h"

@implementation LocalitySearchVCModel

- (void)update
{
    self->_localities = [NSMutableArray new];
    __weak typeof(self) this = self;
    [self.networkManager loadPlacesWithName:self.localityName onSuccess:^(NSData * _Nonnull data) {
        NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSArray * features = [dic[@"result"][@"address"] firstObject][@"features"];
        for (NSDictionary * feature in features)
        {
            NSArray * addrComponents = feature[@"properties"][@"address_components"];
            NSMutableArray * newComps = [[NSMutableArray alloc] initWithCapacity:3];
            for (NSDictionary * comp in addrComponents)
            {
                [newComps insertObject:comp[@"value"] atIndex:0];
            }
            NSString * name = [newComps componentsJoinedByString:@", "];
            
            NSArray * coord = [feature[@"geometry"][@"geometries"] firstObject][@"coordinates"];
            NSString * coords = [[[coord reverseObjectEnumerator] allObjects] componentsJoinedByString:@","];
            
            Locality * locality = [[Locality alloc] initWithName:name withCoords:coords];
            [this.localities addObject: locality];
        }
        
        [this.delegate modelDidUpdate];
    } onFail:^(NSError * _Nonnull error) {
        [this.delegate modelDidFailUpdate];
    }];
}
@end

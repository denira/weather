#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Locality : NSObject

@property (readonly) NSString * name;
@property (readonly) NSString * coords;

- (instancetype)initWithName:(NSString *)name withCoords:(NSString *)coords;

@end

NS_ASSUME_NONNULL_END

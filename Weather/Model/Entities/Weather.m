#import "Weather.h"
#import "NetworkRequest.h"

@interface Weather ()

@property NSString * imageUrl;

@end

@implementation Weather

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    if (self = [super init])
    {
        self->_weatherDesc = [dict[@"lang_ru"] firstObject][@"value"];
        self->_wind = [dict[@"windspeedKmph"] doubleValue];
        self->_pressure = [dict[@"pressure"] doubleValue];

        if (dict[@"temp_C"]) self->_temp = [dict[@"temp_C"] doubleValue];
        else self->_temp = [dict[@"tempC"] doubleValue];
        
        self.imageUrl = dict[@"weatherIconUrl"][0][@"value"];
    }
    return self;
}

- (void)loadImageOnSuccess:(XDataBlock)onSuccess onFail:(XErrorBlock)onFail
{
    NetworkRequest * request = [[NetworkRequest alloc] initWithURL:[NSURL URLWithString:self.imageUrl] withType:@"GET"];
    request.onSuccess = onSuccess;
    request.onFail = onFail;
    [request start];
}
@end

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Helper.h"

@interface Weather : NSObject

@property NSString *date;
@property (readonly) NSString *weatherDesc;
@property (readonly) CGFloat wind;
@property (readonly) CGFloat pressure;
@property (readonly) CGFloat temp;
@property (readonly) UIImage *weatherImage;

-(instancetype)initWithDictionary:(NSDictionary *)dict;
- (void)loadImageOnSuccess:(XDataBlock)onSuccess onFail:(XErrorBlock)onFail;

@end

#import "Locality.h"

@implementation Locality

- (instancetype)initWithName:(NSString *)name withCoords:(NSString *)coords
{
    if (self = [super init])
    {
        self->_name = name;
        self->_coords = coords;
    }
    return self;
}

@end

#import <Foundation/Foundation.h>
#import "CommonVCModel_Protected.h"

@class Weather;

NS_ASSUME_NONNULL_BEGIN

@interface DetailedWeatherVCModel : CommonVCModel

@property NSString * date;
@property (readonly) NSMutableArray <Weather *> *weathers;

@end

NS_ASSUME_NONNULL_END

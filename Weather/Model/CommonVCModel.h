#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class NetworkManager;

@protocol CommonVCModelDelegate <NSObject>

- (void)modelDidUpdate;
- (void)modelDidFailUpdate;

@end

@interface CommonVCModel : NSObject

- (instancetype)initWithDelegate:(id<CommonVCModelDelegate>)delegate;
- (void)update;

@end

NS_ASSUME_NONNULL_END

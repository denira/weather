#import "CommonVCModel.h"
#import "CommonVCModel_Protected.h"
#import "NetworkManager.h"

@implementation CommonVCModel

- (instancetype)initWithDelegate:(id<CommonVCModelDelegate>)delegate
{
    self = [super init];
    if (self)
    {
        self.delegate = delegate;
        self.networkManager = [NetworkManager new];
    }
    return self;
}

- (void)update
{
    NSAssert(self, @"This method should be overridden in subclasses");
}
@end

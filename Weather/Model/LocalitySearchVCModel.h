#import <Foundation/Foundation.h>
#import "CommonVCModel_Protected.h"
#import "Helper.h"

NS_ASSUME_NONNULL_BEGIN

@class Locality;

@interface LocalitySearchVCModel : CommonVCModel

@property NSString * localityName;
@property (readonly) NSMutableArray <Locality *> * localities;
@end

NS_ASSUME_NONNULL_END

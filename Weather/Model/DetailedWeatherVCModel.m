#import "DetailedWeatherVCModel.h"
#import "Constants.h"
#import "Weather.h"
#import "NetworkManager.h"
#import "Helper.h"
#import "Locality.h"

@implementation DetailedWeatherVCModel

- (void)update
{
    self->_weathers = [[NSMutableArray alloc] initWithCapacity:24/HOUR_INTERVAL];
    
    NSString *coords = [Helper getSavedLocality].coords;
    if (!coords) coords = DEFAULT_COORDS;
    
    __weak typeof(self) this = self;
    [self.networkManager loadWeatherByHoursForCoords:coords forDate:self.date onSuccess:^(NSData * _Nonnull data) {
        NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSArray * days = [dic[@"data"][@"weather"] firstObject][@"hourly"];
        for (NSDictionary * dict in days)
        {
            Weather * weather = [[Weather alloc] initWithDictionary: dict];
            NSInteger time = [dict[@"time"] integerValue];
            //время приходит в формате 0, 900, 2100
            [weather setDate:[NSString stringWithFormat:@"%d:00", (int)time/100]];
            [this.weathers addObject:weather];
        }
        [this.delegate modelDidUpdate];
    } onFail:^(NSError * _Nonnull error) {
        [this.delegate modelDidFailUpdate];
    }];
}

@end

#import <Foundation/Foundation.h>
#import "CommonVCModel_Protected.h"

@class Weather;

@interface GeneralVCModel : CommonVCModel

@property (readonly) Weather *weatherToday;
@property (readonly) NSMutableArray <Weather *> *weathers;

@end

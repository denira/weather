#import "NetworkManager.h"
#import "NetworkRequest.h"
#import "Constants.h"

@implementation NetworkManager

- (void)loadWeatherForDaysForCoords:(NSString *)coords onSuccess:(XDataBlock)onSuccess onFail:(XErrorBlock)onFail
{
    NSDictionary * params = @{
                              @"key": W_API_KEY,
                              @"q": coords,
                              @"format": @"json",
                              @"num_of_days": @(DAYS),
                              @"tp": @"24",
                              @"lang": @"ru"
                              };
    NSString * urlString = [self urlStringWithString:W_API_URL withParams:params isNeedFormat:NO];
    NetworkRequest * request = [[NetworkRequest alloc] initWithURL: [NSURL URLWithString: urlString] withType:@"GET"];
    request.onSuccess = onSuccess;
    request.onFail = onFail;
    [request start];
}

- (void)loadWeatherByHoursForCoords:(NSString *)coords forDate:(NSString *)date onSuccess:(XDataBlock)onSuccess onFail:(XErrorBlock)onFail
{
    NSDictionary * params = @{
                              @"key": W_API_KEY,
                              @"q": coords,
                              @"format": @"json",
                              @"date": date,
                              @"tp": @(HOUR_INTERVAL),
                              @"lang": @"ru"
                              };
    NSString * urlString = [self urlStringWithString:W_API_URL withParams:params isNeedFormat:NO];
    NetworkRequest * request = [[NetworkRequest alloc] initWithURL: [NSURL URLWithString: urlString] withType:@"GET"];
    request.onSuccess = onSuccess;
    request.onFail = onFail;
    [request start];
}

- (void)loadPlacesWithName:(NSString *)placeName onSuccess:(XDataBlock)onSuccess onFail:(XErrorBlock)onFail
{
    NSDictionary * params = @{
                              @"q": placeName
                              };
    
    NSString * urlString = [self urlStringWithString:GEO_API_URL withParams:params isNeedFormat:YES];
    NetworkRequest * request = [[NetworkRequest alloc] initWithURL: [NSURL URLWithString: urlString] withType:@"GET"];
    request.onSuccess = onSuccess;
    request.onFail = onFail;
    [request start];
}

- (NSString *)urlStringWithString:(NSString *)string withParams:(NSDictionary *)params isNeedFormat:(BOOL)isNeedFormat
{
    NSMutableArray *parameterArray = [NSMutableArray array];
    
    [params enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSObject *obj, BOOL *stop) {
        NSString *param;
        NSString * object = (NSString *)obj;
        param = [NSString stringWithFormat:@"%@=%@", key, object];
        [parameterArray addObject:param];
    }];
    
    NSString * parameters = [parameterArray componentsJoinedByString:@"&"];
    
    if (isNeedFormat) parameters = [self percentEscapeString:parameters];

    return [string stringByAppendingString:parameters];
}

- (NSString *)percentEscapeString:(NSString *)string {
    NSCharacterSet *allowed = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~="];
    return [string stringByAddingPercentEncodingWithAllowedCharacters:allowed];
}
@end

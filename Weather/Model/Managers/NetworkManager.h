#import <Foundation/Foundation.h>
#import "Helper.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetworkManager : NSObject

- (void)loadWeatherForDaysForCoords:(NSString *)coords onSuccess:(XDataBlock)onSuccess onFail:(XErrorBlock)onFail;

- (void)loadWeatherByHoursForCoords:(NSString *)coords forDate:(NSString *)date onSuccess:(XDataBlock)onSuccess onFail:(XErrorBlock)onFail;

- (void)loadPlacesWithName:(NSString *)placeName onSuccess:(XDataBlock)onSuccess onFail:(XErrorBlock)onFail;

@end

NS_ASSUME_NONNULL_END

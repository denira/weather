#import "GeolocationManager.h"
#import <MapKit/MapKit.h>
#import "Locality.h"
#import "Helper.h"

@interface GeolocationManager () <CLLocationManagerDelegate>

@property CLLocationManager *locationManager;
@property (nonatomic, weak) id<GeolocationManagerDelegate> delegate;
@property BOOL isNeedUpdate;
@end

@implementation GeolocationManager

+ (instancetype)shared
{
    static GeolocationManager * _sharedInstance;
    if(!_sharedInstance) {
        static dispatch_once_t oncePredicate;
        dispatch_once(&oncePredicate, ^{
            _sharedInstance = [self new];
            _sharedInstance.locationManager = [[CLLocationManager alloc] init];
            _sharedInstance.locationManager.delegate = _sharedInstance;
            _sharedInstance.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        });
    }
    return _sharedInstance;
}

- (void)setDelegate:(id<GeolocationManagerDelegate>)delegate
{
    _delegate = delegate;
}

- (void)updateCoords
{
    self.isNeedUpdate = YES;
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    switch (status) {
        case kCLAuthorizationStatusDenied:
            [self.delegate managerStatusIsDenied];
            break;
            
        case kCLAuthorizationStatusNotDetermined:
            [self.locationManager requestWhenInUseAuthorization];
            break;
            
        default:
            [self startUpdate];
            break;
    }
}

- (void)startUpdate
{
    if (self.isNeedUpdate)
    {
        [self.delegate managerDidStartUpdate];
        [self.locationManager startUpdatingLocation];
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) [self startUpdate];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    self.isNeedUpdate = NO;
    [self.delegate coordinatesDidFailReceived];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(nonnull NSArray<CLLocation *> *)locations
{
    self.isNeedUpdate = NO;
    CLLocation *currentLocation = locations[0];
    if (currentLocation != nil) {
        NSString * coords = [NSString stringWithFormat:@"%f,%f", currentLocation.coordinate.latitude, currentLocation.coordinate.longitude];
        
        [self.locationManager stopUpdatingLocation];
        
        __weak typeof(self) this = self;
        CLGeocoder *reverseGeocoder = [CLGeocoder new];
        [reverseGeocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
            if (error) [this.delegate coordinatesDidFailReceived];
            else
            {
                CLPlacemark * placemark = [placemarks firstObject];
                NSString *LocalityName = placemark.locality;
                if (!LocalityName) LocalityName = placemark.administrativeArea;
                
                [Helper saveLocality:[[Locality alloc] initWithName:LocalityName withCoords:coords]];
                
                [this.delegate coordinatesDidReceived];
            }
        }];
    }
    else [self.delegate coordinatesDidFailReceived];
}
@end

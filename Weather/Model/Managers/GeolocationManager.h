#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class Locality;

@protocol GeolocationManagerDelegate <NSObject>

- (void)managerDidStartUpdate;
- (void)managerStatusIsDenied;
- (void)coordinatesDidReceived;
- (void)coordinatesDidFailReceived;

@end

@interface GeolocationManager : NSObject

+ (instancetype)shared;
- (void)setDelegate:(id<GeolocationManagerDelegate>)delegate;
- (void)updateCoords;

@end

NS_ASSUME_NONNULL_END

#import "GeneralVCModel.h"
#import "Constants.h"
#import "Weather.h"
#import "NetworkManager.h"
#import "Locality.h"

@implementation GeneralVCModel

- (void)update
{
    NSString *coords = [Helper getSavedLocality].coords;
    if (!coords) coords = DEFAULT_COORDS;
    
    self->_weathers = [[NSMutableArray alloc] initWithCapacity:DAYS];
    
    __weak typeof(self) this = self;
    [self.networkManager loadWeatherForDaysForCoords:coords onSuccess:^(NSData * _Nonnull data) {
        NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSMutableArray * days = [NSMutableArray arrayWithArray: dic[@"data"][@"weather"]];
        self->_weatherToday = [[Weather alloc] initWithDictionary: [[days firstObject][@"hourly"] firstObject]];
        if (days.count) [days removeObjectAtIndex:0];
        for (NSDictionary * dict in days)
        {
            Weather * weather = [[Weather alloc] initWithDictionary: [dict[@"hourly"] firstObject]];
            [weather setDate:dict[@"date"]];
            [this.weathers addObject:weather];
        }
        [this.delegate modelDidUpdate];
    } onFail:^(NSError * _Nonnull error) {
        [this.delegate modelDidFailUpdate];
    }];
}

@end

#import "HoursTVCell.h"
#import "Weather.h"
#import "Helper.h"

@interface HoursTVCell ()
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@end
@implementation HoursTVCell

- (void)setWeather:(Weather *)weather
{
    _weather = weather;
    self.dateLabel.text = weather.date;
    self.tempLabel.text =[NSString stringWithFormat:@"%0.1f°С", weather.temp];
    self.descLabel.text = weather.weatherDesc;
    
    __weak typeof(self) this = self;
    [weather loadImageOnSuccess:^(NSData * _Nonnull data) {
        UIImage * image = [UIImage imageWithData:data];
        [Helper runOnMainThread:^{
            this.iconView.image = image;
        }];
    } onFail:^(NSError * _Nonnull error) {
        //
    }];
}

@end


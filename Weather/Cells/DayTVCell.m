#import "DayTVCell.h"
#import "Weather.h"

@interface DayTVCell ()

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *windLabel;
@property (weak, nonatomic) IBOutlet UILabel *pressureLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempLabel;

@end

@implementation DayTVCell

- (void)setWeather:(Weather *)weather
{
    _weather = weather;
    self.dateLabel.text = [NSString stringWithFormat:@"%@", weather.date];
    self.windLabel.text = [NSString stringWithFormat:@"%0.2f км/ч", weather.wind];
    self.pressureLabel.text = [NSString stringWithFormat:@"%0.2f гПа", weather.pressure];
    self.tempLabel.text = [NSString stringWithFormat:@"%0.1f°С", weather.temp];
}
@end

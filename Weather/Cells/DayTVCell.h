#import <UIKit/UIKit.h>

@class Weather;
@interface DayTVCell : UITableViewCell

@property (nonatomic) Weather * weather;

@end

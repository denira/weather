//
//  AppDelegate.h
//  Weather
//
//  Created by Новикова Дарья on 05.06.2018.
//  Copyright © 2018 Новикова Дарья. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

